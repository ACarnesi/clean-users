<#
.SYNOPSIS
Clean-Users removes any local win32_userprofile, excluding any profiles marked as special or included in the exccludedUsers array. 
.DESCRIPTION
Clean-Users  uses WMI to retrieve a list of all non-special user profiles on the local machine. It then filters out any user
which has a name matching an element in the excludedUsers parameter. The program then deletes the remaining user profiles using
the remove-wmiobject cmdlt. 
.PARAMETER excludedUsers
An array of strings used to filter out win32_UserProfiles which are to be excluded from deletion. The string values are
compared against the local path paramater of win32_userprofile objects. A full path can be used, but all that is requried
is the name of the final directory. For example, "username" out of "C:\Users\username"
.EXAMPLE
Clean-Users -excludedUsers (Get-Content "C:\Path\ExcludedUsers.txt") 
#>
[CmdletBinding()]
param (
    [Parameter(Mandatory=$True)]
    [string[]]$excludedUsers
)
$UsersToDelete = Get-WmiObject win32_userprofile -Filter {special = false}
ForEach ($user in $excludedUsers)
{
    $UsersToDelete = $UsersToDelete | Where-Object -FilterScript {$_.localpath -notlike "*$user*"}
}
$UsersToDelete 